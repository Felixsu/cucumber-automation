@runme
Feature: hello-world
  Hello world feature

  Scenario Outline:
    Given hello world
    And some parameter as below
      | name | <name> |
      | age  | <age>  |
    Then finish "<fin_args>"
    Examples:
      | Note     | hiptest-uid                              | name   | age | fin_args |
      | sample-1 | xxxxxx                                   | felix  | 19  | ok       |
      | sample-2 | zzzzzz                                   | lixsu  | 21  | no       |