package com.felixsu.cucumber.glue;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.felixsu.automation.model.Person;
import com.google.inject.Inject;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.Map;

/**
 * Created on 12/18/17.
 *
 * @author felixsoewito
 */
@ScenarioScoped
public class StepA {
    private static final ObjectMapper mapper = new ObjectMapper();

    private SharedStorage sharedStorage;

    @Inject
    public StepA(SharedStorage sharedStorage) {
        this.sharedStorage = sharedStorage;
    }

    @Before
    public void before(Scenario scenario) {
        System.out.println("cucumber before hook");
        System.out.println(String.format("Scenario details: %s-%s", scenario.getId(), scenario.getName()));
    }

    @After
    public void after(Scenario scenario) {
        System.out.println("cucumber after hook");
        if (scenario.isFailed()) {
            System.out.println("scenario failed");
        }
    }

    @Given("hello world")
    public void helloWorld() {
        System.out.println("hello world");
    }

    @Given("some parameter as below")
    public void someParameterAsBelow(Map<String, String> parameter) throws Exception {

        String json = mapper.writeValueAsString(parameter);
        Person p = mapper.readValue(json, Person.class);

        System.out.println(String.format("hi %s %d years old", p.getName(), p.getAge()));

        if (p.getAge() > 20) {
            throw new RuntimeException("too old");
        }
    }

    @Then("finish \"([^\"]*)\"")
    public void finish(String arg1) {
        System.out.println("finish!");
        System.out.println(String.format("finish argument: %s", arg1));
    }


}
