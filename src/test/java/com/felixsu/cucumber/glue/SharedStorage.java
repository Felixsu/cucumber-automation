package com.felixsu.cucumber.glue;

import com.google.inject.Singleton;

/**
 * Created on 12/18/17.
 *
 * @author felixsoewito
 */
@Singleton
public class SharedStorage {

    public void helloFromSharedStorage() {
        System.out.println("hello from shared storage");
    }
}
